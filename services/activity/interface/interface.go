package activityface

import (
	"context"
	"bitbucket.org/innius/go-sdk/services/activity"
)

// ActivityAPI represents the interface for the activity service.
type ActivityAPI interface {
	ActivateTemplate(ctx context.Context, input *activity.ActivateTemplateInput) (*activity.ActivateTemplateOutput, error)
}

var _ ActivityAPI = (*activity.ActivitySvc)(nil)
