package activity

import (
	"bitbucket.org/innius/go-sdk/environment"
	"bitbucket.org/innius/go-sdk/namespace"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
)

type ActivitySvc struct {
	*restclient.RestClient
	resolver.Resolver
}

const serviceName string = "activity-service"

func New(env environment.Environment) *ActivitySvc {
	return &ActivitySvc{
		Resolver:   resolver.NewServiceDiscoveryResolverWithFallback(serviceName, namespace.New(env), namespace.NewLegacy(env)),
		RestClient: restclient.New(authentication.PrivateEndpointAuthentication{}),
	}
}
