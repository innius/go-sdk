package activity

import (
	"context"
	"fmt"

	apicontext "bitbucket.org/innius/go-api-context"
)

const (
	Unknown ActivityStatus = iota
	ToDo
	InProgress
	Done
	Cancelled
	Postponed
)

type (
	ActivityStatus int

	TriggerRequest struct {
		Assignee       string `json:"assignee"`
		ScheduledStart int64  `json:"scheduled_start"` //in millis
		ScheduledEnd   int64  `json:"scheduled_end"`   //in millis
		Activator      string `json:"activator"`
	}

	ActivateTemplateInput struct {
		MachineTrn string `json:"machine_trn"`
		TemplateId string `json:"template_id"`
		Body       TriggerRequest
	}

	ActivateTemplateOutput struct {
		Activity
	}

	Activity struct {
		Machinetrn     string                 `json:"machine_trn"` //Hash Key
		Id             string                 `json:"activity_id"` //Range Key
		Status         ActivityStatus         `json:"status"`
		Type           string                 `json:"activity_type"`
		ScheduledStart int64                  `json:"scheduled_start"` //millis since epoch
		ScheduledEnd   int64                  `json:"scheduled_end"`   //millis since epoch
		ActualStart    int64                  `json:"actual_start"`
		ActualEnd      int64                  `json:"actual_end"`
		Company        string                 `json:"company_id"` //Hash of secondary index. id of company that owns the machine.
		Assignee       string                 `json:"assignee"`   //Hash of secondary index #2. trn of person
		Activator      string                 `json:"activator"`  //The person or sensor that has triggered the activity
		Title          string                 `json:"title"`
		Conversationid string                 `json:"conversation_id"`
		Description    string                 `json:"description"`
		TemplateId     string                 `json:"template_id"`
		History        []StateChange          `json:"history"`
		UserData       map[string]interface{} `json:"user_data, omitempty"`
		Attachments    []string               `json:"attachments, omitempty"`
	}

	StateChange struct {
		Status ActivityStatus `json:"status"`
		Time   int64          `json:"time"`
	}
)

func (c *ActivitySvc) ActivateTemplate(ctx context.Context, input *ActivateTemplateInput) (*ActivateTemplateOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := fmt.Sprintf("/internal/templates/%v/%v", input.MachineTrn, input.TemplateId)
	result := &ActivateTemplateOutput{}
	if _, err := c.Post(apicontext.From(ctx), endpoint.Path(path), input.Body, result); err != nil {
		return nil, err
	}
	return result, nil
}

func (s ActivityStatus) String() string {
	switch s {
	case ToDo:
		return "To Do"
	case InProgress:
		return "In Progress"
	case Done:
		return "Done"
	case Cancelled:
		return "Cancelled"
	case Postponed:
		return "Postponed"
	default:
		return "Unknown"
	}
}
