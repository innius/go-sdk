package analytics_management

import (
	"bitbucket.org/innius/go-sdk/environment"
	"bitbucket.org/innius/go-sdk/namespace"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
)

type AnalyticsManagement struct {
	*restclient.RestClient
	resolver.Resolver
}

const serviceName string = "analytics-management-service"

func New(env environment.Environment) *AnalyticsManagement {
	return &AnalyticsManagement{
		Resolver:   resolver.NewServiceDiscoveryResolverWithFallback(serviceName, namespace.New(env), namespace.NewLegacy(env)),
		RestClient: restclient.New(authentication.PrivateEndpointAuthentication{}),
	}
}
