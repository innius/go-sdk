package analytics_management

import (
	"fmt"
	"net/http"

	"context"

	apicontext "bitbucket.org/innius/go-api-context"
	"bitbucket.org/innius/go-trn"
)

type GetTimeseriesInput struct {
	TRN      *trn.TRN `json:"trn"`
	ShiftID  string   `json:"shiftid"`
	DateFrom int64    `json:"date_from"`
	DateTo   int64    `json:"date_to"`
}

type GetTimeseriesResponse struct {
	Timeboxes []Timebox
}

type Timebox struct {
	Start  int64 `json:"start"`
	Finish int64 `json:"finish"`
}

type GetAllTimeseriesInput struct {
	TRN      string `json:"trn"`
	DateFrom int64  `json:"date_from"`
	DateTo   int64  `json:"date_to"`
}

type GetAllTimeseriesResponse struct {
	Timeboxes []NamedTimebox
}

type NamedTimebox struct {
	Start  int64  `json:"start"`
	Finish int64  `json:"finish"`
	Id     string `json:"shift_id"`
	Name   string `json:"name"`
}

func (c *AnalyticsManagement) GetTimeseriesById(ctx context.Context, input *GetTimeseriesInput) (*GetTimeseriesResponse, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := endpoint.Path(fmt.Sprintf("%s/shift/%s/timeseries", input.TRN, input.ShiftID))
	if input.DateFrom != 0 && input.DateTo != 0 {
		path = path.QueryParam("from_date", fmt.Sprintf("%v", input.DateFrom))
		path = path.QueryParam("to_date", fmt.Sprintf("%v", input.DateTo))
	}

	output := []Timebox{}
	if _, err := c.Get(apicontext.From(ctx), path, &output); err != nil {
		return nil, err
	}
	return &GetTimeseriesResponse{output}, nil
}

func (c *AnalyticsManagement) GetAllTimeseries(ctx context.Context, input *GetAllTimeseriesInput) (*GetAllTimeseriesResponse, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := endpoint.Path(fmt.Sprintf("/internal/%s/shift/timeseries", input.TRN)).
		QueryParam("from_date", fmt.Sprintf("%v", input.DateFrom)).
		QueryParam("to_date", fmt.Sprintf("%v", input.DateTo))

	output := []NamedTimebox{}
	if _, err := c.Get(apicontext.From(ctx), path, &output, http.StatusOK, http.StatusNotFound); err != nil {
		return nil, err
	}
	return &GetAllTimeseriesResponse{output}, nil
}
