package managementiface

import (
	"context"

	analytics_management "bitbucket.org/innius/go-sdk/services/analytics/management"
)

type AnalyticsManagementAPI interface {
	GetTimeseriesById(context.Context, *analytics_management.GetTimeseriesInput) (*analytics_management.GetTimeseriesResponse, error)
	GetAllTimeseries(context.Context, *analytics_management.GetAllTimeseriesInput) (*analytics_management.GetAllTimeseriesResponse, error)
}

var _ AnalyticsManagementAPI = (*analytics_management.AnalyticsManagement)(nil)
