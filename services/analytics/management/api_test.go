package analytics_management

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"testing"

	"bitbucket.org/innius/go-sdk/endpoint"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
	"bitbucket.org/innius/go-trn"
	"github.com/icrowley/fake"
	. "github.com/smartystreets/goconvey/convey"
	"gopkg.in/h2non/gock.v1"
)

func newTestAPI(ep string) *AnalyticsManagement {
	e, _ := endpoint.Parse(ep)
	return &AnalyticsManagement{
		RestClient: restclient.New(authentication.Insecure{}),
		Resolver:   resolver.NewStaticEndpointResolver(e),
	}
}

func TestAnalyticsManagementService(t *testing.T) {
	Convey("Analytics Management Service", t, func() {
		endpoint := "http://" + fake.DomainName()
		client := newTestAPI(endpoint)
		Reset(func() {
			gock.OffAll()
		})
		defer gock.Off()

		machineID := "m1"
		dateFrom := 1
		dateTo := 100

		Convey(" get all timeseries", func() {
			response := []NamedTimebox{
				{Id: "foo", Name: "the box", Start: 0, Finish: 100},
			}

			path := fmt.Sprintf("/internal/%s/shift/timeseries", machineID)
			gock.New(endpoint).
				Get(path).
				MatchParam("from_date", strconv.Itoa(dateFrom)).
				MatchParam("to_date", strconv.Itoa(dateTo)).
				Reply(http.StatusOK).JSON(response)

			c, err := client.GetAllTimeseries(context.Background(), &GetAllTimeseriesInput{TRN: machineID, DateFrom: int64(dateFrom), DateTo: int64(dateTo)})

			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)
		})
		Convey("get timeseries by id", func() {
			response := []Timebox{
				{Start: 1, Finish: 100},
			}
			machineID := trn.NewMachine("foo")
			shiftID := "foo"
			path := fmt.Sprintf("%s/shift/%s/timeseries", machineID, shiftID)

			gock.New(endpoint).
				Get(path).
				MatchParam("from_date", strconv.Itoa(dateFrom)).
				MatchParam("to_date", strconv.Itoa(dateTo)).
				Reply(http.StatusOK).JSON(response)

			res, err := client.GetTimeseriesById(context.Background(), &GetTimeseriesInput{TRN: machineID, ShiftID: shiftID, DateFrom: int64(dateFrom), DateTo: int64(dateTo)})

			So(err, ShouldBeNil)
			So(res, ShouldNotBeNil)
		})
	})
}
