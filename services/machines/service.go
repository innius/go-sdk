package machines

import (
	"bitbucket.org/innius/go-sdk/environment"
	"bitbucket.org/innius/go-sdk/namespace"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
)

type MachineSvc struct {
	*restclient.RestClient
	resolver.Resolver
}

const serviceName string = "machine-service"

func New(env environment.Environment) *MachineSvc {
	return &MachineSvc{
		Resolver:   resolver.NewServiceRecordResolver(serviceName, namespace.New(env)),
		RestClient: restclient.New(authentication.PrivateEndpointAuthentication{}),
	}
}
