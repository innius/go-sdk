package machiniface

import (
	"context"

	"bitbucket.org/innius/go-sdk/services/machines"
)

type MachineAPI interface {
	GetCompany(context.Context, *machines.GetCompanyInput) (*machines.GetCompanyOutput, error)

	GetAllMachinesForPersonPrivate(context.Context, *machines.GetPersonMachinesInput) (*machines.GetPersonMachinesOutput, error)

	GetCompanyPersons(context.Context, *machines.GetCompanyPersonsInput) (*machines.GetCompanyPersonsOutput, error)
	
	CheckMachineAccess(context.Context, string, string) (bool, error)
	//ResolveMachine returns an innius-trn based on external identification
	ResolveMachine(context.Context, *machines.ResolveMachineInput) (string, error)

	// internal endpoint which returns all machines for a specified company
	ListCompanyMachines(ctx context.Context, in *machines.ListMachineInput) ([]machines.ListMachineOutput, error)
	// internal endpoint which returns all relations a specified company
	ListCompanyRelations(ctx context.Context, in *machines.ListRelationInput) ([]machines.ListRelationOutput, error)
	// internal endpoint which returns all machines, for e.g. conversions
	ListAllMachines(ctx context.Context) ([]string, error)
}

var _ MachineAPI = (*machines.MachineSvc)(nil)
