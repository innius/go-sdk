package machines

import (
	apicontext "bitbucket.org/innius/go-api-context"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
)

type (
	GetPersonMachinesInput struct {
		PersonID string
	}

	GetPersonMachinesOutput struct {
		Machines []string `json:"machines"`
	}

	GetCompanyPersonsInput struct {
		CompanyId string
	}

	GetCompanyPersonsOutput struct {
		Persons []GetPersonOutput
	}

	GetPersonOutput struct {
		ID          string `json:"id"`
		Name        string `json:"personname"`
		Email       string `json:"email"`
		PhoneNumber string `json:"phonenumber"`
	}

	ResolveMachineInput struct {
		CompanyID string
		TagName   string
		TagValue  string
	}
)

const (
	GetPersonMachinesCommand = "GetPersonMachinesCommand"
	GetCompanyPersonsCommand = "GetCompanyPersonsCommand"
	CheckSharedMachine       = "CheckSharedMachineCommand"
	ResolveMachineCommand    = "ResolveMachineCommand"
	GetCompanyCommand        = "GetCompanyCommand"
)

func (c *MachineSvc) GetAllMachinesForPersonPrivate(ctx context.Context, input *GetPersonMachinesInput) (*GetPersonMachinesOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}

	path := fmt.Sprintf("internal/%s/machines", input.PersonID)
	out := []string{}
	if _, err := c.Get(apicontext.From(ctx), endpoint.Path(path), &out, http.StatusOK); err != nil {
		return nil, err
	}
	return &GetPersonMachinesOutput{Machines: out}, nil
}

func (c *MachineSvc) GetCompanyPersons(ctx context.Context, input *GetCompanyPersonsInput) (*GetCompanyPersonsOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := fmt.Sprintf("companies/%s/persons", input.CompanyId)
	out := []GetPersonOutput{}
	if _, err := c.Get(apicontext.From(ctx), endpoint.Path(path), &out, http.StatusOK); err != nil {
		return nil, err
	}
	return &GetCompanyPersonsOutput{Persons: out}, nil
}

// CheckSharedMachine checks if a machine is shared with your company
func (c *MachineSvc) CheckMachineAccess(ctx context.Context, companyID, machineID string) (bool, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return false, err
	}
	path := fmt.Sprintf("/internal/machines/%s/company/%s", machineID, companyID)
	resp, err := c.Get(apicontext.From(ctx), endpoint.Path(path), nil, http.StatusOK, http.StatusNotFound, http.StatusForbidden)
	if err != nil {
		return false, err
	}
	return resp.StatusCode == http.StatusOK, nil
}

func (c *MachineSvc) ResolveMachine(ctx context.Context, input *ResolveMachineInput) (string, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return "", err
	}
	path := fmt.Sprintf("companies/%v/tags/%v/%v", input.CompanyID, input.TagName, input.TagValue)
	resp, err := c.Get(apicontext.From(ctx), endpoint.Path(path), nil, http.StatusOK, http.StatusNotFound)
	if err != nil {
		return "", err
	}
	if resp.StatusCode == http.StatusNotFound {
		return "", nil
	}
	defer resp.Body.Close()
	bits, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.Wrap(err, "could not read response body")
	}
	return string(bits), err
}

type ListMachineInput struct {
	// the company id for which the machines are read
	CompanyID string `json:"company_id"`

	// filter which ensures only machines shared by this company are returned
	MachineOwner string `json:"machine_owner"`
}

type ListMachineOutput struct {
	// the trn of the machine
	ID string `json:"id"`

	// the name of the machine
	Name string `json:"displayname"`

	// the type of the machine
	ConnectionStatus string `json:"connection_status"`

	// the description of the machine
	Description string `json:"description"`
}

func (c *MachineSvc) ListCompanyMachines(ctx context.Context, in *ListMachineInput) ([]ListMachineOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	endpoint = endpoint.Path(fmt.Sprintf("/internal/companies/%s/machines", in.CompanyID))
	if in.MachineOwner != "" {
		endpoint = endpoint.QueryParam("c", in.MachineOwner)
	}
	out := []ListMachineOutput{}

	if _, err := c.Get(apicontext.From(ctx), endpoint, &out, http.StatusOK); err != nil {
		return nil, err
	}
	return out, nil
}

// ListRelationInput defines the options for the ListCompanyRelations api
type ListRelationInput struct {
	// the company id for which the machines are read
	CompanyID string

	//
	Domain string
}

type ListRelationOutput struct {
	// the name of the company
	Name string `json:"companyname,omitempty"`

	// the company id of the relation
	ID string `json:"id"`

	// the domain of the relation
	Domain string `json:"domain"`

	// the status of the relation
	Status int `json:"status"`
}

func (c *MachineSvc) ListCompanyRelations(ctx context.Context, in *ListRelationInput) ([]ListRelationOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := fmt.Sprintf("/internal/companies/%s/relations", in.CompanyID)

	type apiResponse []struct {
		Status  int `json:"rstatus"`
		Company struct {
			ID              string `json:"id,omitempty"`
			Name            string `json:"companyname,omitempty"`
			Domain          string `json:"domain,omitempty"`
			Status          int    `json:"cstatus,omitempty"`
			TrialExpiryDate int64  `json:"trial_end_date,omitempty"`
		} `json:"company"`
		Contact string `json:"contact"`
	}
	apiResult := apiResponse{}

	if _, err := c.Get(apicontext.From(ctx), endpoint.Path(path), &apiResult, http.StatusOK); err != nil {
		return nil, err
	}
	res := make([]ListRelationOutput, len(apiResult))
	for i := range apiResult {
		r := apiResult[i]
		res[i] = ListRelationOutput{
			ID:     r.Company.ID,
			Status: r.Status,
			Domain: r.Company.Domain,
			Name:   r.Company.Name,
		}
	}
	return res, nil
}

// TODO: find out what the use case is of this endpoint; Seems to be very strange to return all machines for all companies in one shot
func (c *MachineSvc) ListAllMachines(ctx context.Context) ([]string, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := "/internal/allmachines"
	machines := []string{}
	if _, err := c.Get(apicontext.From(ctx), endpoint.Path(path), &machines, http.StatusOK); err != nil {
		return nil, err
	}
	return machines, nil
}

type GetCompanyInput struct {
	// the innius ID of the company
	ID string

	// the domain of the company
	// cannot be used in conjunction with ID
	Domain string
}

func (r *GetCompanyInput) id() string {
	if len(r.Domain) > 0 {
		return r.Domain
	}
	return r.ID
}

type GetCompanyOutput struct {
	// the id for this company
	//
	// required: true
	// min: 1
	ID string `json:"id,omitempty"`

	// the name for this company
	//
	// required: true
	// min: 1
	Name string `json:"companyname,omitempty"`

	// the domain for this company
	//
	// required: true
	// min: 1
	Domain string `json:"domain,omitempty"`

	// the status of this company
	// value is determined by the backend and has the following options:
	// * 3 = active
	// * 4 = blocked
	// * 6 = trial
	//
	// read-only: true
	Status int `json:"cstatus,omitempty"`

	// the trial period expiration date of the company
	// default trial period is 30 days
	// read-only: true
	TrialExpiryDate int64 `json:"trial_end_date,omitempty"`
}

func (c *MachineSvc) GetCompany(ctx context.Context, in *GetCompanyInput) (*GetCompanyOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := "/internal/companies/" + in.id()
	out := GetCompanyOutput{}
	res, err := c.Get(apicontext.From(ctx), endpoint.Path(path), &out, http.StatusOK, http.StatusNotFound)
	if err != nil {
		return nil, err
	}
	if res.StatusCode == http.StatusNotFound {
		return nil, nil
	}
	return &out, nil
}
