package kpis

import (
	apicontext "bitbucket.org/innius/go-api-context"
	"bitbucket.org/innius/go-sdk/endpoint"
	"context"
	"fmt"
	"net/http"
)

type KPIConfig struct {
	ID         string `json:"kpi"`
	Frequency  string `json:"frequency"`
	Format     string `json:"format"`
	Thresholds []struct {
		Status int     `json:"status"`
		Value  float64 `json:"value"`
	} `json:"thresholds"`
	InputSensors []struct {
		Name       string `json:"name"`
		SensorName string `json:"sensorname"`
	} `json:"inputSensors"`
	CreationTimestamp int64 `json:"creation_timestamp"`
	CalculationTime   []struct {
		Day  int    `json:"day"`
		Time string `json:"time"`
	} `json:"calculation_times"`
	CalculationRange int `json:"calculation_range"`
}

type GetMachineKPIRequest struct {
	MachineID string
}

type GetMachineKPIResponse struct {
	Configs []KPIConfig `json:"configs"`
}

func (c *KPI) GetMachineKpiConfig(ctx context.Context, query *GetMachineKPIRequest) (*GetMachineKPIResponse, error) {
	endpoint, err := c.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := fmt.Sprintf("/internal/%s/config", query.MachineID)
	result := &GetMachineKPIResponse{}
	if _, err := c.Get(apicontext.From(ctx), endpoint.Path(path), result, http.StatusOK); err != nil {
		return nil, err
	}
	return result, nil
}

//TODO: specify a list of kpis to be calculated; is currently not supported by the backend
type CalculateKpisRequest struct {
	MachineID string
	KPI       string
	From      int64
	To        int64
	Shift     string
}

func (r *CalculateKpisRequest) params(ep *endpoint.Endpoint) *endpoint.Endpoint {
	return addQueryParams(ep, r.From, r.To, r.Shift)
}

func addQueryParams(ep *endpoint.Endpoint, from, to int64, shift string) *endpoint.Endpoint {
	if from > 0 {
		ep.QueryParam("from_date", fmt.Sprint(from))
	}
	if to > 0 {
		ep.QueryParam("to_date", fmt.Sprint(to))
	}
	if shift != "" {
		ep.QueryParam("shift_id", shift)
	}

	return ep
}

type CalculatedKpi struct {
	ID             string  `json:"kpi_type"`
	Value          float64 `json:"value"`
	ValueFormat    string  `json:"value_format"`
	FromDate       int64   `json:"from_date"`
	ToDate         int64   `json:"to_date"`
	Status         int     `json:"status"`
	TargetValue    float64 `json:"target_value"`
	TargetFormat   string  `json:"target_format"`
	CriticalValue  float64 `json:"critical_value"`
	TrendType      int     `json:"trend_type"`
	EmergencyValue float64 `json:"emergency_value"`
}

type CalculateKpisResult struct {
	Kpis []CalculatedKpi `json:"kpis"`
}

func (c *KPI) CalculateKpis(ctx context.Context, query *CalculateKpisRequest) (*CalculateKpisResult, error) {
	path := fmt.Sprintf("/internal/%s/kpis", query.MachineID)
	if query.KPI != "" {
		path += "/" + query.KPI
	}
	result := &CalculateKpisResult{}
	endpoint, err := c.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	if _, err := c.Get(apicontext.From(ctx), query.params(endpoint.Path(path)), result, http.StatusOK); err != nil {
		return nil, err
	}
	return result, nil
}

type CalculateKpiDetailsRequest struct {
	MachineID string
	KPI       string
	From      int64
	To        int64
	Shift     string
}

func (r *CalculateKpiDetailsRequest) params(p *endpoint.Endpoint) *endpoint.Endpoint {
	return addQueryParams(p, r.From, r.To, r.Shift)
}

type CalculatedKpiDetailValue struct {
	Timestamp int64   `json:"timestamp"`
	Value     float64 `json:"value"`
}

type CalculatedKpiDetail struct {
	CalculatedKpi
	TimestampedValues []TimestampedValue `json:"details"`
}

type TimestampedValue struct {
	Timestamp int64   `json:"timestamp"`
	Value     float64 `json:"value"`
}

func (c *KPI) CalculateKpiDetails(ctx context.Context, query *CalculateKpiDetailsRequest) ([]CalculatedKpiDetail, error) {
	endpoint, err := c.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := fmt.Sprintf("/internal/%s/kpis/%s/details", query.MachineID, query.KPI)

	result := []CalculatedKpiDetail{}
	if _, err := c.Get(apicontext.From(ctx), query.params(endpoint.Path(path)), &result, http.StatusOK); err != nil {
		return nil, err
	}
	return result, nil
}
