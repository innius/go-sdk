package kpis

import (
	"bitbucket.org/innius/go-sdk/environment"
	"bitbucket.org/innius/go-sdk/namespace"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
)

type KPI struct {
	*restclient.RestClient
	resolver.Resolver
}

const serviceName string = "kpi-service"

func New(env environment.Environment) *KPI {
	return &KPI{
		Resolver:   resolver.NewServiceRecordResolver(serviceName, namespace.New(env)),
		RestClient: restclient.New(authentication.PrivateEndpointAuthentication{}),
	}
}
