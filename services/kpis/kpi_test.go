package kpis

import (
	"bitbucket.org/innius/go-sdk/endpoint"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_CalculateKpisRequest_Params(t *testing.T) {
	ep, _ := endpoint.Parse("https://10.0.1.2:50054/kpis")

	sut := CalculateKpisRequest{
		From:      10,
		To:        20,
		Shift:     "foo",
	}

	result := sut.params(ep)

	assert.Equal(t, "https://10.0.1.2:50054/kpis?from_date=10&shift_id=foo&to_date=20", result.String())

}
