package kpisiface

import (
	"bitbucket.org/innius/go-sdk/services/kpis"
	"context"
)

// KPIAPI provides sdk capabilities for the innius kpi service
type KPIAPI interface {
	// Get the kpi configuration for a machine
	GetMachineKpiConfig(context.Context, *kpis.GetMachineKPIRequest) (*kpis.GetMachineKPIResponse, error)

	// Calculate kpis for a machine
	CalculateKpis(context.Context, *kpis.CalculateKpisRequest) (*kpis.CalculateKpisResult, error)

	// Calculate the kpi details for a machine
	CalculateKpiDetails(context.Context, *kpis.CalculateKpiDetailsRequest) ([]kpis.CalculatedKpiDetail, error)
}

var _ KPIAPI = (*kpis.KPI)(nil)
