package usages

import (
	"fmt"
	"strings"

	"bitbucket.org/innius/httpsig"

	"bitbucket.org/innius/go-sdk/endpoint"
	"bitbucket.org/innius/go-sdk/environment"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
)

type UsageClient struct {
	*restclient.RestClient
	resolver.Resolver
}

const serviceName = "usage-service"

func New(key *httpsig.DataKey, env environment.Environment) *UsageClient {
	rest := restclient.New(authentication.SignedRequestAuthentication{
		RequestSigner: httpsig.NewRequestSigner(key),
	})

	ep, _ := endpoint.Parse(apiGatewayEndpoint(env))

	return &UsageClient{
		RestClient: rest,
		Resolver:   resolver.NewStaticEndpointResolver(ep),
	}
}

func apiGatewayEndpoint(env environment.Environment) string {
	basepath := serviceName
	envpath := ""
	switch env {
	case environment.Test:
		envpath = ".test"
	case environment.Demo:
		envpath = ".demo"
	}
	if !strings.HasPrefix(basepath, "/") {
		basepath = "/" + basepath
	}
	basepath = strings.TrimSuffix(basepath, "/")
	return fmt.Sprintf("https://api%s.innius.com%s", envpath, basepath)
}
