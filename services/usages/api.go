package usages

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	apicontext "bitbucket.org/innius/go-api-context"
)

func (u *UsageClient) GetUsage(c context.Context, companyID string, endDate int64) ([]UsageDto, error) {
	endpoint, err := u.Resolver.Resolve(c)
	if err != nil {
		return nil, err
	}
	endpoint = endpoint.Path(fmt.Sprintf("/companies/%s", companyID))
	endpoint = endpoint.QueryParam("timestamp", fmt.Sprintf("%v", endDate))

	if companyID == "" {
		return nil, errors.New("company ID must be specified")
	}

	ctx := apicontext.WithCompanyID(apicontext.From(c), companyID)

	resp := []UsageDto{}

	if _, err := u.Get(ctx, endpoint, &resp, http.StatusOK); err != nil {
		return nil, err
	}

	return resp, nil
}

type UsageDto struct {
	CompanyID    string       `json:"company_id"`
	ResourceType ResourceType `json:"resource_type"`
	ResourceName ResourceName `json:"resource_name"`
	Quantity     float64      `json:"quantity"`
}

type ResourceType string

const (
	Sensor ResourceType = "sensor"
)

type ResourceName string

const (
	HighSensor   ResourceName = "high_frequency_sensor"
	FastSensor                = "fast_frequency_sensor"
	NormalSensor              = "normal_frequency_sensor"
	MediumSensor              = "medium_frequency_sensor"
	LowSensor                 = "low_frequency_sensor"
)
