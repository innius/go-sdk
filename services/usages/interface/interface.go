package usagiface

import (
	"bitbucket.org/innius/go-sdk/services/usages"
	"context"
)

type UsageAPI interface {
	GetUsage(c context.Context, companyID string, endDate int64) ([]usages.UsageDto, error)
}