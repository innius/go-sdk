package sensors

import "bitbucket.org/innius/go-trn"

const (
	Physical SensorKind = "physical"
	Virtual  SensorKind = "virtual"

	Continuous SensorDataType = "continuous"
	Discrete   SensorDataType = "discrete"
	GPS        SensorDataType = "gps"
)

type (
	SensorKind string

	SensorDataType string

	GetDefinitionInput struct {
		ID *trn.TRN
	}

	GetDefinitionOutput struct {
		MachineDefinition
	}

	GetSensorDefinitionInput struct {
		MachineTrn *trn.TRN
		SensorID   string
	}

	GetSensorDefinitionOutput struct {
		SensorDefinition
	}

	MachineDefinition struct {
		Id      string             `json:"id"`
		Name    string             `json:"name"`
		Sensors []SensorDefinition `json:"sensors"`
	}

	SensorDefinition struct {
		ID           string          `json:"id"`
		Machine      string          `json:"machinetrn"`
		Name         string          `json:"name"`
		Type         string          `json:"type"`
		Description  string          `json:"description, omitempty"`
		Value        ValueDefinition `json:"value"`
		Capacity     int             `json:"capacity"`
		Thresholds   []Threshold     `json:"thresholds"`
		Tags         interface{}     `json:"tags"`
		PhysicalId   string          `json:"physicalid"`
		Rate         int             `json:"rate"`
		Kind         SensorKind      `json:"kind"`
		Visible      bool            `json:"visible"`
		Propagate    bool            `json:"propagate"`
		Origin       string          `json:"origin"`
		Timebuckets  interface{}     `json:"timebuckets"`
		Transient    bool            `json:"transient"`
		Aliases      []Alias         `json:"aliases"`
		BatchSensor  string          `json:"batch"`
		RecipeSensor string          `json:"recipe"`
	}

	ValueDefinition struct {
		Type           string         `json:"type"`
		Unit           string         `json:"unitOfMeasure"`
		Decimals       int            `json:"decimalplaces"`
		Missing        string         `json:"missing"`
		SensorDataType SensorDataType `json:"sensor_data_type"`
	}

	Alias struct {
		Value       int    `json:"value"`
		Description string `json:"description"`
	}

	Threshold struct {
		Notification      bool                `json:"notification"`
		DurationInSeconds int                 `json:"durationInSeconds"`
		Debouncer         int                 `json:"debouncer"`
		Condition         string              `json:"condition"`
		Type              string              `json:"type"`
		Expression        ThresholdExpression `json:"thresholdExpression"`
		TemplateID        string              `json:"template_id, omitempty"`
	}

	ThresholdExpression struct {
		Type     string  `json:"expr"`
		Value    float64 `json:"value"`
		IsSensor bool    `json:"isSensor"`
		SensorId string  `json:"sensorid"`
	}
)
