package sensors

import (
	"bitbucket.org/innius/go-sdk/environment"
	"bitbucket.org/innius/go-sdk/namespace"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
)

type SensorSvc struct {
	*restclient.RestClient
	resolver.Resolver
}

const serviceName = "sensor-service"

func New(env environment.Environment) *SensorSvc {
	return &SensorSvc{
		Resolver:   resolver.NewServiceDiscoveryResolverWithFallback(serviceName, namespace.New(env), namespace.NewLegacy(env)),
		RestClient: restclient.New(authentication.PrivateEndpointAuthentication{}),
	}
}
