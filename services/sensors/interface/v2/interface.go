package sensoriface

import (
	"context"

	"bitbucket.org/innius/go-sdk/services/sensors"
)

type SensorAPI interface {
	// GetMachineSensors returns all sensors for a given machine specification
	GetMachineSensors(context.Context, *sensors.GetMachineSensorsInput) (*sensors.GetDefinitionOutput, error)
	// GetSensor return a sensor definition or nil if it does not exist
	GetSensor(ctx context.Context, input *sensors.GetSensorInput) (*sensors.GetSensorDefinitionOutput, error)
}

var _ SensorAPI = (*sensors.SensorSvc)(nil)
