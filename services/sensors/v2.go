package sensors

import (
	"context"
	"fmt"
	"net/http"

	apicontext "bitbucket.org/innius/go-api-context"
)

// GetMachineSensorsInput defines the machine for which sensors are read
type GetMachineSensorsInput struct {
	// ID is the internal identification (trn) of an innius machine
	ID string
}

// GetMachineSensorsInput defines the machine for which sensors are read
type GetSensorInput struct {
	// ID is the internal identification (trn) of an innius machine
	MachineID string
	SensorID  string
}

func (c *SensorSvc) GetMachineSensors(ctx context.Context, input *GetMachineSensorsInput) (*GetDefinitionOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}

	path := fmt.Sprintf("/internal/%s/eventing/definition", input.ID)
	out := GetDefinitionOutput{}
	if _, err := c.Get(apicontext.From(ctx), endpoint.Path(path), &out, http.StatusOK); err != nil {
		return nil, err
	}
	return &out, nil
}

func (c *SensorSvc) GetSensor(ctx context.Context, input *GetSensorInput) (*GetSensorDefinitionOutput, error) {
	endpoint, err := c.Resolver.Resolve(ctx)
	if err != nil {
		return nil, err
	}
	path := fmt.Sprintf("/internal/%s/eventing/definition/%s", input.MachineID, input.SensorID)
	out := GetSensorDefinitionOutput{}
	resp, err := c.Get(apicontext.From(ctx), endpoint.Path(path), &out, http.StatusOK, http.StatusNotFound)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == http.StatusNotFound {
		return nil, nil
	}
	return &out, nil
}
