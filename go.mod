module bitbucket.org/innius/go-sdk

require (
	bitbucket.org/innius/go-api-context v1.1.5
	bitbucket.org/innius/go-trn v1.0.4
	bitbucket.org/innius/httpsig v1.0.4
	github.com/corpix/uarand v0.0.0-20181120140456-37c9140f52f6 // indirect
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/google/uuid v1.1.0 // indirect
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/nbio/st v0.0.0-20140626010706-e9e8d9816f32 // indirect
	github.com/pkg/errors v0.8.0
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	github.com/stretchr/testify v1.9.0
	github.com/taskcluster/slugid-go v1.0.1-0.20180913150312-f39bc6d245da
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.27.0
	gopkg.in/h2non/gock.v1 v1.0.12
)

exclude bitbucket.org/to-increase/go-mountebank v0.0.0-20181119101429-73eeff9f738e

go 1.13
