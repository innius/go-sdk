package sensortest

import (
	"context"
	"net/http"
	"testing"

	"bitbucket.org/innius/go-sdk/endpoint"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
	"bitbucket.org/innius/go-sdk/services/sensors"
	"github.com/icrowley/fake"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/taskcluster/slugid-go/slugid"
	"gopkg.in/h2non/gock.v1"
)

func newTestAPI(ep string) *sensors.SensorSvc {
	e, _ := endpoint.Parse(ep)
	return &sensors.SensorSvc{
		RestClient: restclient.New(authentication.Insecure{}),
		Resolver:   resolver.NewStaticEndpointResolver(e),
	}
}
func TestSensorAPI(t *testing.T) {
	Convey("Sensor API", t, func() {

		apiEndpoint := "http://" + fake.DomainName()
		client := newTestAPI(apiEndpoint)
		Reset(func() {
			gock.OffAll()
		})
		defer gock.Off()
		Convey(" Get the sensor definition", func() {
			body := `
			{
			"id" : "foo",
			"name" : "the foo sensor"
			}
			`
			gock.New(apiEndpoint).Reply(http.StatusOK).JSON(body)
			input := &sensors.GetSensorInput{
				SensorID: "foo",
			}
			c, err := client.GetSensor(context.Background(), input)

			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)
			So(c.ID, ShouldEqual, "foo")

			Convey(" Get the 404", func() {
				gock.New(apiEndpoint).Reply(http.StatusNotFound)

				c, err := client.GetSensor(context.Background(), input)
				So(err, ShouldBeNil)
				So(c, ShouldBeNil)
			})
		})
		Convey("Get the machine sensors", func() {
			body := `
			{
			"id" : "my-super-machine",
			"sensors" : [
					{	
						"id" : "foo", 
						"name" : "the foo sensor"
					}
				]
			}
			`
			input := &sensors.GetMachineSensorsInput{
				ID: slugid.Nice(),
			}
			gock.New(apiEndpoint).Reply(http.StatusOK).JSON(body)
			c, err := client.GetMachineSensors(context.Background(), input)

			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)
			So(c.Id, ShouldEqual, "my-super-machine")
			So(c.Sensors, ShouldHaveLength, 1)
		})
	})
}
