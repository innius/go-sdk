package machine_tests

import (
	"bitbucket.org/innius/go-sdk/endpoint"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"
	"bitbucket.org/innius/go-sdk/services/machines"
	"context"
	"fmt"
	"github.com/icrowley/fake"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/taskcluster/slugid-go/slugid"
	"gopkg.in/h2non/gock.v1"
	"net/http"
	"testing"
)

func newTestAPI(ep string) *machines.MachineSvc {
	e, _ := endpoint.Parse(ep)
	return &machines.MachineSvc{
		RestClient: restclient.New(authentication.Insecure{}),
		Resolver:   resolver.NewStaticEndpointResolver(e),
	}
}
func TestMachineAPI(t *testing.T) {
	Convey("Machine API", t, func() {

		apiEndpoint := "http://" + fake.DomainName()
		client := newTestAPI(apiEndpoint)
		Reset(func() {
			gock.OffAll()
		})
		defer gock.Off()
		company := &machines.GetCompanyOutput{ID: slugid.Nice(), Domain: fake.DomainName()}
		Convey(" Get the company", func() {
			gock.New(apiEndpoint).Reply(http.StatusOK).JSON(company)
			c, err := client.GetCompany(context.Background(), &machines.GetCompanyInput{ID: company.ID})

			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)
			So(c.ID, ShouldEqual, company.ID)
			So(c.Domain, ShouldEqual, company.Domain)

			Convey(" Get the 404", func() {
				gock.New(apiEndpoint).Reply(http.StatusNotFound)
				c, err := client.GetCompany(context.Background(), &machines.GetCompanyInput{ID: company.ID})
				So(err, ShouldBeNil)
				So(c, ShouldBeNil)
			})
		})
		Convey("List relations", func() {
			body := `
				[{
					"rstatus" : 1, 
					"company" : {
						"id" : "foo",
						"domain" : "foo.com", 
                        "companyname" : "bar"
					}
				}]
			`

			gock.New(apiEndpoint).Reply(http.StatusOK).JSON(body)
			c, err := client.ListCompanyRelations(context.Background(), &machines.ListRelationInput{CompanyID: company.ID})

			So(err, ShouldBeNil)
			So(c, ShouldHaveLength, 1)
			So(c[0].ID, ShouldEqual, "foo")
			So(c[0].Domain, ShouldEqual, "foo.com")
			So(c[0].Name, ShouldEqual, "bar")
			So(c[0].Status, ShouldEqual, 1)
		})

		Convey("List machines", func() {
			owner := slugid.Nice()
			body := `
				[
					{	
						"id" : "foo", 
						"name" : "the foo machine"
					},
					{	
						"id" : "bar", 
						"name" : "the bar machine"
					}
				]
			`
			gock.New(apiEndpoint).Get("/internal/companies/"+company.ID+"/machines").
				MatchParam("c", owner).
				Reply(http.StatusOK).JSON(body)

			res, err := client.ListCompanyMachines(context.Background(), &machines.ListMachineInput{CompanyID: company.ID, MachineOwner: owner})
			So(err, ShouldBeNil)
			So(res, ShouldHaveLength, 2)
		})
		Convey("Resolve Machine", func() {
			tagName := "color"
			tagValue := fake.Color()
			machineID := fake.ProductName()
			path := fmt.Sprintf("/companies/%s/tags/%s/%s", company.ID, tagName, tagValue)
			gock.New(apiEndpoint).Get(path).Reply(http.StatusOK).BodyString(machineID)
			c, err := client.ResolveMachine(context.Background(), &machines.ResolveMachineInput{CompanyID: company.ID, TagValue: tagValue, TagName: tagName})

			So(err, ShouldBeNil)
			So(c, ShouldEqual, machineID)
		})
	})
}
