package kpis_test

import (
	"testing"

	"gopkg.in/h2non/gock.v1"

	"bitbucket.org/innius/go-sdk/endpoint"
	"bitbucket.org/innius/go-sdk/resolver"
	"bitbucket.org/innius/go-sdk/restclient"
	"bitbucket.org/innius/go-sdk/restclient/authentication"

	"net/http"

	"context"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"bitbucket.org/innius/go-sdk/services/kpis"
)

const portNumber = 5555

func newTestAPI(ep string) *kpis.KPI {
	e, _ := endpoint.Parse(ep)
	return &kpis.KPI{
		RestClient: restclient.New(authentication.Insecure{}),
		Resolver:   resolver.NewStaticEndpointResolver(e),
	}
}

func TestGetMachineKpiConfig(t *testing.T) {
	defer gock.Off()
	r := &kpis.GetMachineKPIResponse{
		Configs: []kpis.KPIConfig{
			{
				ID: "MTBF",
				Thresholds: []struct {
					Status int     "json:\"status\""
					Value  float64 "json:\"value\""
				}{{}, {}},
				CreationTimestamp: 17,
			},
			{
				ID: "MTBF",
				Thresholds: []struct {
					Status int     "json:\"status\""
					Value  float64 "json:\"value\""
				}{{}, {}},
				CreationTimestamp: 17,
			},
			{
				ID: "OEE",
				Thresholds: []struct {
					Status int     "json:\"status\""
					Value  float64 "json:\"value\""
				}{{}, {}},
				InputSensors: []struct {
					Name       string `json:"name"`
					SensorName string `json:"sensorname"`
				}{{}, {}, {}, {}},
				CreationTimestamp: 17,
			},
		},
	}
	apiEndpoint := "http://" + fake.DomainName()
	client := newTestAPI(apiEndpoint)

	query := &kpis.GetMachineKPIRequest{MachineID: "foo"}
	gock.New(apiEndpoint).Reply(http.StatusOK).JSON(r)

	res, err := client.GetMachineKpiConfig(context.Background(), query)

	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Len(t, res.Configs, 3)
	for _, cfg := range res.Configs {
		assert.NotEmpty(t, cfg.ID)
		if cfg.ID == "OEE" {
			assert.Len(t, cfg.InputSensors, 4)
		}
		assert.Len(t, cfg.Thresholds, 2)
		assert.NotEmpty(t, cfg.CreationTimestamp)
	}
}

func TestCalculateKpi(t *testing.T) {
	defer gock.Off()
	r := &kpis.CalculateKpisResult{
		Kpis: []kpis.CalculatedKpi{
			{
				ID:    "Quality",
				Value: 1.0,
			},
			{
				ID:    "Performance",
				Value: 1.0,
			},
			{
				ID:    "Availability",
				Value: 1.0,
			},
			{
				ID:    "OEE",
				Value: 1.0,
			},
		},
	}

	query := &kpis.CalculateKpisRequest{MachineID: "foo"}
	apiEndpoint := "http://" + fake.DomainName()

	gock.New(apiEndpoint).Reply(http.StatusOK).JSON(r)
	client := newTestAPI(apiEndpoint)
	res, err := client.CalculateKpis(context.Background(), query)

	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Len(t, res.Kpis, 4)
	for _, kpi := range res.Kpis {
		assert.NotEmpty(t, kpi.ID)
		assert.NotEmpty(t, kpi.Value)
	}
}

func TestCalculateKpiDetails(t *testing.T) {
	defer gock.Off()
	r := []kpis.CalculatedKpiDetail{
		{
			CalculatedKpi: kpis.CalculatedKpi{
				ID:    "Quality",
				Value: 1.0,
			},
			TimestampedValues: []kpis.TimestampedValue{{}},
		},
		{
			CalculatedKpi: kpis.CalculatedKpi{
				ID:    "Performance",
				Value: 1.0,
			},
			TimestampedValues: []kpis.TimestampedValue{{}},
		},
		{
			CalculatedKpi: kpis.CalculatedKpi{
				ID:    "Availability",
				Value: 1.0,
			},
			TimestampedValues: []kpis.TimestampedValue{{}},
		},
		{
			CalculatedKpi: kpis.CalculatedKpi{
				ID:    "OEE",
				Value: 100.0,
			},
			TimestampedValues: []kpis.TimestampedValue{{}},
		},
	}
	req := &kpis.CalculateKpiDetailsRequest{
		MachineID: "foo",
		KPI:       "bar",
		From:      30,
		To:        20,
		Shift:     "baz",
	}
	apiEndpoint := "http://" + fake.DomainName()
	client := newTestAPI(apiEndpoint)

	gock.New(apiEndpoint).Reply(http.StatusOK).JSON(r)
	res, err := client.CalculateKpiDetails(context.Background(), req)

	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Len(t, res, 4)
	for _, kpi := range res {
		assert.NotEmpty(t, kpi.ID)
		assert.NotEmpty(t, kpi.Value)
	}
}
