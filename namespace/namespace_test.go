package namespace

import (
	"bitbucket.org/innius/go-sdk/environment"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew(t *testing.T) {
	ns := New(environment.Test)
	assert.Equal(t, "services.chair.private", string(ns))
}
