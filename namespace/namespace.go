package namespace

import (
	"bitbucket.org/innius/go-sdk/environment"
	"fmt"
)

type Namespace string

// New returns a new namespace
func New(env environment.Environment) Namespace {
	return Namespace(fmt.Sprintf(`services.%s.private`, env))
}

// NewLegacy returns a new legacy namespace for services which do not support the new namespace
// Deprecated: migrate service to latest namespace and use New()
func NewLegacy(env environment.Environment) Namespace {
	return Namespace(fmt.Sprintf("%s.servicediscovery.internal", env))
}
