package restclient

import (
	apicontext "bitbucket.org/innius/go-api-context"
	"bitbucket.org/innius/go-sdk/endpoint"
	authentication2 "bitbucket.org/innius/go-sdk/restclient/authentication"
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
)

func New(auth authentication2.Scheme) *RestClient {
	return &RestClient{
		Client: http.DefaultClient,
		Scheme: auth,
	}
}

type RestClient struct {
	authentication2.Scheme
	*http.Client
}

// Post performs an POST action on the specified url Note that response body will be closed if v != nil!
func (c RestClient) Post(ctx apicontext.ApiContext, url *endpoint.Endpoint, body interface{}, v interface{}, expected ...int) (*http.Response, error) {
	req, err := c.newHttpRequest(ctx, http.MethodPost, url.String(), body)
	if err != nil {
		return nil, err
	}
	return c.sendRequest(req, v, expected...)
}

func (c RestClient) Get(ctx apicontext.ApiContext, url *endpoint.Endpoint, v interface{}, expected ...int) (*http.Response, error) {
	req, err := c.newHttpRequest(ctx, http.MethodGet, url.String(), nil)
	if err != nil {
		return nil, err
	}
	return c.sendRequest(req, v, expected...)
}

func (c *RestClient) Delete(ctx apicontext.ApiContext, url *endpoint.Endpoint, v interface{}, expected ...int) (*http.Response, error) {
	req, err := c.newHttpRequest(ctx, http.MethodDelete, url.String(), v)
	if err != nil {
		return nil, err
	}
	return c.sendRequest(req, nil, expected...)
}

func (c *RestClient) sendRequest(r *http.Request, v interface{}, expected ...int) (*http.Response, error) {

	res, err := c.Client.Do(r)
	if err != nil {
		return nil, errors.Wrap(err, "http request failed")
	}
	if len(expected) == 0 {
		expected = append(expected, http.StatusOK)
	}
	statusOK := false
	for _, s := range expected {
		if res.StatusCode == s {
			statusOK = true
			break
		}
	}
	if !statusOK {
		return nil, errors.Errorf("%s returned unexpected status %v; path: ", r.URL.String(), res.StatusCode)
	}
	if v != nil {
		defer res.Body.Close()
		b, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, errors.Wrap(err, "could not read the response body")
		}
		if len(b) > 0 {
			if err := errors.Wrap(json.Unmarshal(b, v), "could not unmarshal the response body"); err != nil {
				return nil, err
			}
		}
	}
	return res, nil
}

func (c *RestClient) newHttpRequest(ctx apicontext.ApiContext, method, url string, v interface{}) (*http.Request, error) {
	body, err := encodeBody(v)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set(http.CanonicalHeaderKey("content-type"), "application/json")

	return c.Scheme.Apply(ctx, req.WithContext(ctx))
}

func encodeBody(v interface{}) (io.Reader, error) {
	var b *bytes.Buffer
	if v == nil {
		return nil, nil
	}
	b = &bytes.Buffer{}
	if err := json.NewEncoder(b).Encode(v); err != nil {
		return nil, errors.Wrap(err, "could not encode the request body")
	}
	return b, nil
}
