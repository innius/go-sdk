package authentication

import (
	apicontext "bitbucket.org/innius/go-api-context"
	"bitbucket.org/innius/httpsig"
	"net/http"
)

type Scheme interface {
	Apply(c apicontext.ApiContext, r *http.Request) (*http.Request, error)
}

type Insecure struct{}

func (p Insecure) Apply(c apicontext.ApiContext, r *http.Request) (*http.Request, error) {
	return r, nil
}

// PrivateEndpointAuthentication allows services to communicate via private endpoints
// this SecurityScheme is considered as deprecated; use SignedRequestAuthentication instead
type PrivateEndpointAuthentication struct {
}

func (p PrivateEndpointAuthentication) Apply(c apicontext.ApiContext, r *http.Request) (*http.Request, error) {
	return r, nil
}

type SignedRequestAuthentication struct {
	*httpsig.RequestSigner
}

func (p SignedRequestAuthentication) Apply(c apicontext.ApiContext, r *http.Request) (*http.Request, error) {
	if err := p.Sign(r); err != nil {
		return nil, err
	}
	return r, nil
}

//type JsonWebTokenAuthentication struct{}
//
//func (p JsonWebTokenAuthentication) Apply(c apicontext.ApiContext, r *http.Request) (*http.Request, error) {
//	panic("Not Implemented")
//}
//
//type APIKeyAuthentication struct {
//}
//
//func (p APIKeyAuthentication) Apply(c apicontext.ApiContext, r *http.Request) (*http.Request, error) {
//	panic("Not Implemented")
//}
