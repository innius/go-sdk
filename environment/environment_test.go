package environment

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseEnvironment(t *testing.T) {
	tests := []struct {
		alias string
		env   Environment
	}{
		{"chair", Test},
		{"CHAIR", Test},
		{"test", Test},
		{"oak", Demo},
		{"demo", Demo},
		{"steam", Live},
		{"prod", Live},
		{"live", Live},
	}

	for _, tc := range tests {
		env, err := Parse(tc.alias)
		assert.NoError(t, err)
		assert.Equal(t, tc.env, env)
	}
	t.Run("parse unknown alias", func(t *testing.T) {
		_, err := Parse("foo")
		assert.Error(t, err)
	})
}
