package environment

import (
	"fmt"
	"strings"
)

type Environment string

const (
	Test Environment = "chair"
	Demo Environment = "oak"
	Live Environment = "steam"
)

func Parse(s string) (Environment, error) {
	switch strings.ToLower(s) {
	case "chair", "test":
		return Test, nil
	case "oak", "demo":
		return Demo, nil
	case "steam", "live", "prod":
		return Live, nil
	default:
		return "", fmt.Errorf("%s is not a reference to an innius environment", s)
	}
}
