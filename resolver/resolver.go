package resolver

import (
	"context"
	"fmt"
	"net"

	"bitbucket.org/innius/go-sdk/endpoint"
	"bitbucket.org/innius/go-sdk/namespace"
	"github.com/pkg/errors"
)

type Resolver interface {
	Resolve(ctx context.Context) (*endpoint.Endpoint, error)
}

// netResolver provides a local interface which mimics net.Resolver
type netResolver interface {
	LookupSRV(ctx context.Context, service, proto, name string) (cname string, addrs []*net.SRV, err error)
}

// NewServiceDiscoveryResolverWithFallback creates a new Resolver with service discovery and fallback support.
//
// This factory function allows the creation of a resolver that combines service discovery with the ability to fallback
// to default resolution mechanisms in case service discovery fails. It utilizes the provided service name and a list
// of namespaces to perform service discovery.
//
// Parameters:
//   - service: The name of the service for which resolution is required.
//   - namespaces: A variable number of namespace.Namespace objects representing the namespaces to search during service discovery.
//     Namespace information is utilized to locate the service in a distributed system.
//
// Returns:
//   - Resolver: A Resolver instance with service discovery and fallback capabilities.
//
// Example:
//
//	resolver := NewServiceDiscoveryResolverWithFallback("example-service", namespace.DefaultNamespace, namespace.SecondaryNamespace)
//	// Use the resolver for service resolution operations.
//
// Note:
//
//	The net.DefaultResolver is used as the default resolver for fallback scenarios.
//	It is recommended to import and use this factory function instead of creating the resolver directly.
func NewServiceDiscoveryResolverWithFallback(service string, namespaces ...namespace.Namespace) Resolver {
	return newServiceDiscoveryResolverWithFallback(net.DefaultResolver, service, namespaces...)
}

// internal factory which allows injection of a custom netResolver
func newServiceDiscoveryResolverWithFallback(netResolver netResolver, service string, namespaces ...namespace.Namespace) Resolver {
	resolvers := make([]Resolver, len(namespaces))
	for i := range namespaces {
		resolvers[i] = &srvRecordResolver{
			Service:   service,
			Namespace: namespaces[i],
			Resolver:  netResolver,
		}
	}
	return &fallbackResolver{
		resolvers:   resolvers,
		netResolver: netResolver,
	}
}

// fallbackResolver is a decorator which
type fallbackResolver struct {
	resolvers   []Resolver
	netResolver netResolver
}

func (r *fallbackResolver) Resolve(ctx context.Context) (*endpoint.Endpoint, error) {
	var lastErr error
	for i := range r.resolvers {
		ep, err := r.resolvers[i].Resolve(ctx)
		if err == nil {
			return ep, nil
		}
		lastErr = err
	}
	return nil, lastErr
}

// NewServiceRecordResolver creates a new dns SRV record resolver
func NewServiceRecordResolver(service string, ns namespace.Namespace) Resolver {
	return &srvRecordResolver{
		Service:   service,
		Namespace: ns,
		Resolver:  net.DefaultResolver,
	}
}

type srvRecordResolver struct {
	Service   string
	Namespace namespace.Namespace
	Resolver  netResolver
}

func (r *srvRecordResolver) Resolve(ctx context.Context) (*endpoint.Endpoint, error) {
	_, addrs, err := r.Resolver.LookupSRV(ctx, r.Service, "tcp", string(r.Namespace))
	if err != nil {
		return nil, errors.Wrapf(err, "lookup SRV record failure for %s", r.Service)
	}

	for _, addr := range addrs {
		return endpoint.Parse(fmt.Sprintf("http://%s:%d", addr.Target, addr.Port))
	}
	return nil, errors.Errorf("No endpoint found for %s", r.Service)
}

// NewStaticEndpointResolver retuns a new static endpoint resolver which can be used in unit tests
func NewStaticEndpointResolver(e *endpoint.Endpoint) Resolver {
	return &staticEndpointResolver{
		Endpoint: e,
	}
}

type staticEndpointResolver struct {
	*endpoint.Endpoint
}

func (r *staticEndpointResolver) Resolve(ctx context.Context) (*endpoint.Endpoint, error) {
	return r.Endpoint, nil
}
