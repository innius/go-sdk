package resolver

import (
	"context"
	"errors"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type testResolver struct {
	mock.Mock
}

func (t *testResolver) LookupSRV(ctx context.Context, service, proto, name string) (cname string, addrs []*net.SRV, err error) {
	args := t.Called(ctx, service, proto, name)
	if x, ok := args.Get(1).([]*net.SRV); ok {
		return args.String(0), x, args.Error(2)
	}
	return "", nil, args.Error(2)
}

func TestNewServiceDiscoveryResolverWithFallback(t *testing.T) {
	const service = "foo_service"
	const primary = "services.bar.com"
	const fallback = "fallback.services.bar.com"
	addrs := []*net.SRV{
		{
			Target: "10.0.0.2",
			Port:   uint16(50050),
		},
	}

	t.Run("primary resolver returns an address", func(t *testing.T) {
		r := &testResolver{}
		r.On("LookupSRV", mock.Anything, service, "tcp", primary).
			Return("", addrs, nil).Once()

		sut := newServiceDiscoveryResolverWithFallback(r, service, primary, fallback)

		res, err := sut.Resolve(context.TODO())

		assert.NoError(t, err)

		assert.Equal(t, "http://10.0.0.2:50050", res.String())

		r.AssertExpectations(t)
	})
	t.Run("primary resolver returns an error", func(t *testing.T) {
		r := &testResolver{}
		r.On("LookupSRV", mock.Anything, service, "tcp", primary).
			Return("", nil, errors.New("primary not found")).Once()
		r.On("LookupSRV", mock.Anything, service, "tcp", fallback).
			Return("", addrs, nil).Once()

		sut := newServiceDiscoveryResolverWithFallback(r, service, primary, fallback)

		res, err := sut.Resolve(context.TODO())

		assert.NoError(t, err)

		assert.Equal(t, "http://10.0.0.2:50050", res.String())

		r.AssertExpectations(t)
	})
	t.Run("both resolvers return an error", func(t *testing.T) {
		r := &testResolver{}
		r.On("LookupSRV", mock.Anything, service, "tcp", primary).
			Return("", nil, errors.New("primary not found")).Once()

		r.On("LookupSRV", mock.Anything, service, "tcp", fallback).
			Return("", nil, errors.New("fallback not found")).Once()

		sut := newServiceDiscoveryResolverWithFallback(r, service, primary, fallback)

		_, err := sut.Resolve(context.TODO())

		assert.Error(t, err)

		r.AssertExpectations(t)
	})
}

func TestNewServiceDiscoveryResolver(t *testing.T) {
	const service = "foo_service"
	const ns = "servicediscovery.bar.com"

	r := &testResolver{}
	addrs := []*net.SRV{
		{
			Target: "10.0.0.2",
			Port:   uint16(50050),
		},
	}
	r.On("LookupSRV", mock.Anything, service, "tcp", ns).Return("", addrs, nil)

	sut := srvRecordResolver{
		Service:   service,
		Namespace: ns,
		Resolver:  r,
	}

	res, err := sut.Resolve(context.TODO())

	assert.NoError(t, err)
	assert.NotNil(t, res)

	assert.Equal(t, "http://10.0.0.2:50050", res.String())
}
