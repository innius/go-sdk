package endpoint

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEndpoint_Path(t *testing.T) {
	ep, err := Parse("https://foo.com/bar")
	assert.NoError(t, err)

	assert.Equal(t, "https://foo.com/bar/baz", ep.Path("/baz").String())
	assert.Equal(t, "https://foo.com/bar/baz", ep.Path("baz").String())

	t.Run("with Query Params", func(t *testing.T) {
		assert.Equal(t, "https://foo.com/bar/baz?read=true", ep.Path("/baz").QueryParam("read", "true").String())
	})
}

func TestParseEndpoint(t *testing.T) {
	ep, err := Parse("https://10.0.0.10:55000/bar")
	assert.NoError(t, err)
	assert.Equal(t, "https://10.0.0.10:55000/bar", ep.String())
}
