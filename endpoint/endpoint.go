package endpoint

import (
	"net/url"
	"path"
)

// Parse returns a new an endpoint from a raw URL or an error if the provided string is not a valid url
func Parse(s string) (*Endpoint, error) {
	u, err := url.Parse(s)
	if err != nil {
		return nil, err
	}
	return &Endpoint{url: u, params: map[string][]string{}}, nil
}

type Endpoint struct {
	url    *url.URL
	path   string
	params url.Values
}

func (e *Endpoint) Path(p string) *Endpoint {
	e.path = p
	return e
}

func (e *Endpoint) QueryParam(name, value string) *Endpoint {
	e.params.Add(name, value)
	return e
}

func (e *Endpoint) String() string {
	u := *e.url
	u.Path = path.Join(u.Path, e.path)
	u.RawQuery = e.params.Encode()
	return u.String()
}
